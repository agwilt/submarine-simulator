import math

from defines import *

class VesselType:
    def __init__(self,
                 crush_depth,               # m
                 max_speed,                 # m/s
                 max_course_change,         # rad/s per m/s
                 max_depth_change,          # m/s per m/s
                 deceleration,              # drag accel. = - deceleration * speed
                 acceleration,              # m/s2
                 num_torpedo_tubes,
                 hull_noise_level,
                 engine_noise_level,
                 ping_noise_level,
                 width,                     # m
                 length,                    # m
                 height,                    # m
                 sonar_min_detect_level,
                 search_cone,               # rad, everything apart from baffles for subs
                 name):
        self.crush_depth = crush_depth
        self.max_speed = max_speed
        self.max_course_change = max_course_change
        self.max_depth_change = max_depth_change
        self.deceleration = deceleration
        self.acceleration = acceleration
        self.num_torpedo_tubes = num_torpedo_tubes
        self.hull_noise_level = hull_noise_level
        self.engine_noise_level = engine_noise_level
        self.ping_noise_level = ping_noise_level
        self.width = width
        self.length = length
        self.height = height
        self.sonar_min_detect_level = sonar_min_detect_level
        self.search_cone = search_cone
        self.name = name

VESSEL_BUOY = VesselType(
    0, # min z
    0, 0, 0, 0, 0, # various speeds
    0, # num torpedo tubes
    10, # hull noise level
    0, 0, # engine & active ping noise level
    1, 1, 2, # width, length, height
    0, # sonar min detect level
    0, # search cone
    "buoy"
)

VESSEL_CARGO = VesselType(
    0, # min z
    10, # max speed
    math.pi/1800, # max course change
    0, # depth change speed
    0.005, 0.05, # deceleration, acceleration
    0, # num torpedo tubes
    80, # hull noise level
    100, 0, # engine & active ping noise level
    32, 300, 40, # width, length, height
    0, # sonar min detect level
    0, # search cone
    "cargo"
)

VESSEL_SUB = VesselType(
    -600, # min z
    18, # max speed
    math.pi/1440, # max course change
    0.7, # depth change speed
    0.003, 0.1, # deceleration, acceleration
    6, # num torpedo tubes
    20, # hull noise level
    20, # engine noise level
    200, # active ping noise level
    8, 60, 10, # width, length, height
    0, # sonar min detect level
    math.radians(135), # search cone -- i.e. 45 baffles in either dir
    "sub"
)

VESSEL_TORPEDO = VesselType(
    MAP_BOTTOM, # min z
    25, # max speed
    math.pi/1000, # max course change
    0.7, # depth change speed
    0.001, 0.1, # deceleration, acceleration
    0, # num torpedo tubes
    10, # hull noise level
    50, # engine noise level
    200, # active ping noise level
    0.5, 5, 0.5, # width, length, height
    0, # sonar min detect level
    math.radians(45), # search cone
    "torpedo"
)

# VESSEL_WHALE
# VESSEL_SUBMARINE
VESSEL_TYPES = {VESSEL_BUOY, VESSEL_CARGO, VESSEL_SUB, VESSEL_TORPEDO}

def vessel_type_from_string(name):
    for vessel_type in VESSEL_TYPES:
        if vessel_type.name == name:
            return vessel_type
    raise ValueError("Bad vessel type: %s" % name)
