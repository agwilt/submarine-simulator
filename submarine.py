#!/usr/bin/env python3

import sys
import socket
import readline

from vessel_type import *

def recv_line(sock):
    line = b''
    while True:
        try:
            c = sock.recv(1)
        except ConnectionResetError:
            c = b''
        if c == b'\n':
            break
        elif c == b'':
            print("ERROR: Server disconnected!")
            exit(1)
        else:
            line += c
    return line.decode()

def command_completer(commands):
    def compl(text, state):
        viable_commands = list(filter(lambda s : s[:len(text)] == text, commands))
        try:
            return viable_commands[state] + " "
        except IndexError:
            return None
    return compl

def play(sock):
    readline.clear_history()
    readline.set_completer(command_completer(
        ["end", "skip", "course", "speed", "depth", "scope", "sonar", "ai", "shoot"]))
    readline.parse_and_bind('Control-n: "end\n"')

    skip_turns = 0
    while True:
        formatted_time = recv_line(sock)
        if skip_turns > 0:
            sock.sendall(b"end\n")
            skip_turns -= 1
            continue
        while True:
            try:
                command = input(formatted_time + " > ")
            except EOFError:
                return
            parts = command.split()
            if len(parts) == 0:
                continue
            elif parts[0] == "ai":
                sock.sendall(b"ai\n")
                return
            elif parts[0] == "end" or parts[0] == "skip":
                if parts[0] == "skip" and len(parts) > 1:
                    try:
                        skip_turns = int(parts[1]) - 1
                    except ValueError:
                        print("WARNING: Interpreting \"%s\" as 1" % parts[1])
                sock.sendall(b"end\n")
                break
            sock.sendall(command.encode() + b'\n')

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: %s logfile_directory" % sys.argv[0])
        exit(1)
    address = sys.argv[1] + "/server.socket"

    readline.clear_history()
    readline.parse_and_bind('set editing-mode vi')
    readline.parse_and_bind('set completion-ignore-case on')
    readline.parse_and_bind('tab: complete')

    print("Available vessel types:", ", ".join([t.name for t in VESSEL_TYPES]))
    print("Commands:")
    print("    list")
    print("    new vessel_type player_name [x y [depth [course [speed]]]]")
    print("    take vessel_name")

    while True:
        readline.set_completer(command_completer(["list", "new", "take"]))
        try:
            command = input("> ")
            if command == '':
                continue
            s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            s.setblocking(True)
            s.connect(address)
            s.sendall(command.encode() + b'\n')
            if command[:3] == "new" or command[:4] == "take":
                feedback = recv_line(s)
                if feedback[0] == 'E':
                    print("ERROR:", feedback[2:])
                elif feedback[0] == 'M':
                    print("Message from server:", feedback[2:])
                    play(s)
                    print("")
                else:
                    print("ERROR: Unexpected greeting from server. Exiting ..")
            else:
                for line in s.makefile().readlines():
                    print(line, end='')
            s.close()
        except EOFError:
            s.close()
            exit(0)

