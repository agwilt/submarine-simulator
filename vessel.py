import random
import math
import sys

from defines import *
from vessel_type import *

class Vessel:
    x = 0
    y = 0
    z = 0
    course = 0
    speed = 0

    z_goal = 0
    course_goal = 0
    speed_goal = 0

    scope_up = False
    sonar_active = False
    sonar_contacts = []
    sonar_contact_depths = []

    wrecked = False

    name = None # None for computer-controlled
    socket = None
    output_file = None
    vessel_type = VESSEL_BUOY

    num_seconds_left = None

    def __init__(self, time, vessel_type, name, output_file = None, socket = None):
        self.time = time
        self.vessel_type = vessel_type
        self.name = name
        if output_file != None:
            self.output_file = output_file
            print("", file=self.output_file)
            self.log("TYPE: " + vessel_type.name)
        if socket != None:
            self.socket = socket
        if self.vessel_type == VESSEL_TORPEDO:
            num_seconds_left = 2000

    def set_initial_nav(self, x, y, z, course, speed):
        self.x = x
        self.y = y
        self.z = self.z_goal = z
        self.course = self.course_goal = course
        self.speed = self.speed_goal = speed

    def set_random_initial_nav(self, min_x, max_x, min_y, max_y):
        self.set_initial_nav(random.uniform(min_x, max_x),
                             random.uniform(min_y, max_y),
                             random.uniform(self.vessel_type.crush_depth, 0),
                             random.uniform(0, random.TWOPI),
                             self.vessel_type.max_speed / 3)

    def set_z(self, z):
        self.z = z
        self.z_goal = z

    def set_speed(self, speed):
        self.speed = speed
        self.speed_goal = speed

    def set_course(self, course):
        self.course = course
        self.course_goal = course

    def make_wreck(self):
        self.z_goal = float('-inf')
        self.course_goal = self.course
        self.speed_goal = 0
        self.sonar_active = False
        self.sonar_contacts = []
        self.sonar_contact_depths = []
        self.name = "Wreck (" + self.name + ")"
        self.wrecked = True
        self.log("WARNING: Ship has been wrecked")
        if self.output_file != None:
            self.output_file.close()
            self.output_file = None
        if self.socket != None:
            self.socket.close()
            self.socket = None
        self.num_seconds_left = 3600 # 1h

    def noise_level(self):
        return (self.vessel_type.hull_noise_level * (self.speed + 1)
                + self.vessel_type.engine_noise_level * self.speed_goal
                + self.vessel_type.ping_noise_level * self.sonar_active)

    # TODO: Change all this and use Numpy classes
    def x_y_dist_squared(self, other_vessel):
        return (other_vessel.x - self.x)**2 + (other_vessel.y - self.y)**2

    def distance_squared(self, other_vessel):
        return x_y_dist_squared(self, other_vessel) + (other_vessel.z - self.z)**2

    def can_hear_at_bearing(self, vessel):
        bearing = math.atan2(vessel.x - self.x, vessel.y - self.y) % random.TWOPI
        relative_bearing = ((bearing - self.course + math.pi) % random.TWOPI) - math.pi
        if abs(relative_bearing) >= self.vessel_type.search_cone:
            return None
        return bearing # for now
        #if (other_vessel.noise_level() / (distance_squared(other_vessel) + 1))
        #        >= self.vessel_type.sonar_min_detect_level:

    def process_contacts(self, vessels):
        self.sonar_contacts = []
        self.sonar_contact_depths = []
        for vessel in vessels:
            if vessel == self:
                continue
            bearing = self.can_hear_at_bearing(vessel)
            if bearing != None:
                self.sonar_contacts.append(bearing)
                self.sonar_contact_depths.append(vessel.z)
                self.log("CONTACT: %s [%s], bearing %03d, depth %d" % (
                    vessel.name,
                    vessel.vessel_type.name,
                    math.degrees(bearing),
                    round(-vessel.z)))

    def simulate_time(self, seconds):
        # move
        dx = math.sin(self.course) * self.speed * seconds
        dy = math.cos(self.course) * self.speed * seconds
        self.x += dx
        self.y += dy
        # change depth
        max_depth_change = self.vessel_type.max_depth_change * seconds * (self.speed + 1)
        if self.z < self.z_goal:
            self.z += min(max_depth_change, self.z_goal - self.z)
        elif self.z > self.z_goal:
            self.z -= min(max_depth_change, self.z - self.z_goal)
            if (self.wrecked):
                self.z -= 0.5 * seconds
        self.z = min(self.z, 0)
        # change course
        course_change = ((self.course_goal - self.course + math.pi) % random.TWOPI) - math.pi
        max_course_change = self.vessel_type.max_course_change * self.speed
        if course_change > 0:
            self.course += min(max_course_change * seconds, course_change)
        else:
            self.course -= min(max_course_change * seconds, -course_change)
        self.course %= random.TWOPI
        # change speed
        if self.speed < self.speed_goal:
            self.speed += min(self.vessel_type.acceleration * seconds,
                              self.speed_goal - self.speed)
        elif self.speed > self.speed_goal:
            self.speed -= min(self.vessel_type.deceleration * self.speed * seconds,
                              self.speed - self.speed_goal)
        self.speed = max(min(self.speed, self.vessel_type.max_speed), 0)
        # check if anything went wrong
        if self.z < self.vessel_type.crush_depth and not self.wrecked:
            self.log("WARNING: Exceeded crush depth!")
            self.make_wreck()
        if self.num_seconds_left != None:
            self.num_seconds_left -= seconds

    # return value: new vessels to add to game (i.e. torpedoes)
    def get_input(self):
        if self.socket == None:
            return self.get_input_ai()
        else:
            try:
                self.socket.sendall(("[" + self.formatted_time() + "]\n").encode())
            except BrokenPipeError:
                self.log("WARNING: Lost connection to bridge!")
                self.make_wreck()
                return []
            vessels_to_add = []
            while self.read_command_from_socket(vessels_to_add): pass
            return vessels_to_add

    def get_input_ai(self):
        if self.vessel_type == VESSEL_TORPEDO:
            return self.run_ai_torpedo()
        else:
            return self.run_ai_passive_npc()

    def run_ai_torpedo(self):
        self.speed_goal = self.vessel_type.max_speed
        if len(self.sonar_contacts) > 0:
            self.course_goal = self.sonar_contacts[0]
            self.z_goal = self.sonar_contact_depths[0]
        return []

    def run_ai_autopilot(self):
        return []

    def run_ai_passive_npc(self):
        self.speed_goal = self.vessel_type.max_speed
        if self.z_goal == self.z:
            self.z_goal = random.uniform(self.vessel_type.crush_depth, 0)
        if random.random() > 0.95:
            self.course_goal = (self.course + random.gauss(0, math.pi/2)) % random.TWOPI
        return []

    def read_command_from_socket(self, vessels_to_add):
        line = b''
        while True:
            try:
                c = self.socket.recv(1)
            except ConnectionResetError:
                c = b''
            if c == b'\n':
                break
            elif c == b'':
                self.log("WARNING: Ship scuttled!")
                self.make_wreck()
                return False
            else:
                line += c
        line = line.decode()
        parts = line.split()
        if len(parts) == 0 or parts[0] == "end":
            return False
        try:
            if parts[0] == "course":
                self.course_goal = math.radians(float(parts[1])) % random.TWOPI
                self.log("CONN: Moving to course to %03d" % math.degrees(self.course_goal))
            elif parts[0] == "speed":
                self.speed_goal = min(self.vessel_type.max_speed, float(parts[1]))
                self.log("CONN: Changing speed to %.2f" % self.speed_goal)
            elif parts[0] == "depth":
                self.z_goal = -float(parts[1])
                self.log("CONN: Changing depth to %.2f" % -self.z_goal)
            elif parts[0] == "scope":
                if parts[1][0] == "u":
                    self.scope_up = True
                    self.log("CONN: Up scope")
                elif parts[1][0] == "d":
                    self.scope_false = True
                    self.log("CONN: Down scope")
                else:
                    self.warning_invalid_instruction(line)
            elif parts[0] == "sonar":
                if parts[1][0] == "a":
                    self.sonar_active = True
                    self.log("CONN: Sonar active")
                elif parts[1][0] == "p":
                    self.sonar_active = False
                    self.log("CONN: Sonar passive")
                else:
                    self.warning_invalid_instruction(line)
            elif parts[0] == "shoot": # args: bearing depth
                bearing = math.radians(float(parts[1]))
                depth = float(parts[2])
                vessels_to_add.append(
                        Vessel(self.time, VESSEL_TORPEDO, "torpedo (%s)" % self.name)
                )
                dx = math.sin(self.course) * self.vessel_type.length
                dy = math.cos(self.course) * self.vessel_type.length
                vessels_to_add[-1].set_initial_nav(self.x + dx,
                                                   self.y + dy,
                                                   self.z,
                                                   self.course,
                                                   self.speed + VESSEL_TORPEDO.max_speed)
                vessels_to_add[-1].speed_goal = VESSEL_TORPEDO.max_speed
                vessels_to_add[-1].z_goal = -depth
                vessels_to_add[-1].course_goal = bearing
                self.log("CONN: Launched torpedo! Bearing %03d, depth %d" %
                         (math.degrees(bearing), round(depth)))
            elif parts[0] == "ai":
                self.log("WARNING: Handing control over to AI")
                self.socket.close()
                self.socket = None
                return False
            else:
                self.warning_invalid_instruction(line)
        except ValueError:
            self.warning_invalid_instruction(line)
        return True

    def cleanup(self):
        self.log("WARNING: Vessel removed from game")
        if self.socket != None:
            self.socket.close()
        if self.output_file != None:
            self.output_file.close()

    def formatted_time(self):
        t = self.time()
        s = t % 60
        t //= 60
        m = t % 60
        t //= 60
        h = t % 24
        t //= 24
        return "%dT%02d:%02d:%02d" % (t,h,m,s)

    def log(self, message):
        if self.output_file != None:
            print("["+self.formatted_time()+"]", message, file=self.output_file, flush=True)

    def write_output(self):
        self.log("COURSE: %d" % round(math.degrees(self.course)))
        self.log("SPEED: %.2f m/s" % self.speed)
        self.log("DEPTH: %d m" % round(-self.z))
        self.log("POSITION: (%d m, %d m)" % (round(self.x), round(self.y)))

    def warning_invalid_instruction(self, instruction):
        self.log("WARNING: Invalid instruction: %s" % instruction)
