#!/usr/bin/env python3

import sys
import os
import random
import math
from time import sleep
import socket
import select
import itertools

from vessel import *

def best_within_interval(x, min_x, max_x):
    return max(min(x, max_x), min_x)

def open_output_file(logfile_directory, vessel_name):
    return open(logfile_directory + "/" + vessel_name.replace('/','_') + ".log", "a")

def greet_new_player(vessels, i):
    if vessels[i].socket != None:
        vessels[i].socket.sendall(
            ("M Current players: " + ", ".join(formatted_list(vessels)) + "\n").encode()
        )

def formatted_list(vessels):
    def nice_name(vessel):
        s = vessel.name
        if vessel.wrecked:
            return s
        if vessel.socket == None:
            s += " (AI)"
        s += " [" + vessel.vessel_type.name + "]"
        return s
    return [nice_name(vessel) for vessel in vessels]

def handle_new_connection(vessels, client_socket, time, logfile_directory):
    command = client_socket.makefile().readline().strip()
    command_parts = command.split()
    if len(command_parts) > 2 and command_parts[0] == "new":
        return add_new_vessel(command_parts, vessels, client_socket, time, logfile_directory)
    elif len(command_parts) == 1 and command_parts[0] == "list":
        try:
            client_socket.sendall((", ".join(formatted_list(vessels)) + "\n").encode())
        except BrokenPipeError:
            return "Client broke off connection prematurely"
        client_socket.close()
        return "List of vessels queried"
    elif len(command_parts) >= 2 and command_parts[0] == "take":
        def first_of_name(s):
            i = s.find(' ')
            while s[i] == ' ':
                i += 1
            return i
        vessel_name = command[first_of_name(command):]
        for i, vessel in enumerate(vessels):
            if vessel.socket == None and vessel.name == vessel_name:
                vessel.socket = client_socket
                try:
                    greet_new_player(vessels, i)
                except BrokenPipeError:
                    vessel.socket = None
                    return "Client broke off connection prematurely"
                if vessel.output_file == None:
                    vessel.output_file = open_output_file(logfile_directory, vessel.name)
                return "Player took over vessel " + vessel.name
        try:
            client_socket.sendall(("E Vessel " + vessel_name + " not available.\n").encode())
        finally:
            return "Player tried to take over unavailable vessel " + vessel_name
    else:
        try:
            client_socket.sendall(b"E Bad greeting.\n")
            client_socket.close()
        finally:
            return "Bad connection with command \"" + " ".join(command_parts) + "\""

def add_new_vessel(command_parts, vessels, client_socket, time, logfile_directory):
    try:
        v_type = vessel_type_from_string(command_parts[1])
    except ValueError:
        try:
            client_socket.sendall(b"E Bad vessel type.\n")
            client_socket.close()
        finally:
            return "Bad connection: Vessel type " + command_parts[1] + " doesn't exist"
    v_name = command_parts[2]
    if v_name in [v.name for v in vessels]:
        try:
            client_socket.sendall(b"E Name already taken.\n")
            client_socket.close()
        finally:
            return "Bad connection: Name \"" + v_name + "\" already taken"
    vessels.append(
        Vessel(
            time,
            v_type,
            v_name,
            open_output_file(logfile_directory, v_name),
            client_socket
        )
    )
    try:
        greet_new_player(vessels, -1)
    except BrokenPipeError:
        vessels.pop()
        return "New player %s broke off connection prematurely" % command_parts[2]
    vessels[-1].set_random_initial_nav(MAP_MIN_X, MAP_MAX_X, MAP_MIN_Y, MAP_MAX_Y)
    if len(command_parts) > 3:
        vessels[-1].x = float(command_parts[3])
    if len(command_parts) > 4:
        vessels[-1].y = float(command_parts[4])
    if len(command_parts) > 5:
        vessels[-1].set_z(min(-float(command_parts[5]), 0))
    if len(command_parts) > 6:
        vessels[-1].set_course(math.radians(int(command_parts[6]) % 360))
    if len(command_parts) > 7:
        vessels[-1].set_speed(best_within_interval(float(command_parts[7]),
                                                   0,
                                                   vessels[-1].vessel_type.max_speed))
    if len(command_parts) > 8:
        vessels[-1].log(
            "WARNING: Malformed new vessel declaration \"%s\"" % " ".join(command_parts)
        )
    return "New player: %s" % vessels[-1].name

def check_for_collisions(vessels):
    wreck_vessel = [False for v in vessels]
    vessel_torpedoed = [False for v in vessels]
    for (i1, v1), (i2, v2) in itertools.combinations(enumerate(vessels), 2):
        if abs(v1.z - v2.z) < (v1.vessel_type.height + v2.vessel_type.height):
            r1 = (v1.vessel_type.width + v1.vessel_type.length) / 2
            r2 = (v2.vessel_type.width + v2.vessel_type.length) / 2
            if v1.x_y_dist_squared(v2) < (r1 + r2)**2:
                print("Vessels %s and %s have collided!" % (v1.name, v2.name))
                wreck_vessel[i1] = True
                wreck_vessel[i2] = True
                if v2.vessel_type == VESSEL_TORPEDO:
                    vessel_torpedoed[i1] = True
                if v1.vessel_type == VESSEL_TORPEDO:
                    vessel_torpedoed[i2] = True
    for i in range(len(vessels)-1, -1, -1):
        if wreck_vessel[i]:
            if vessels[i].vessel_type == VESSEL_TORPEDO:
                print("Removing exploded torpedo:", vessels[i].name)
                vessels.pop(i).cleanup()
            else:
                if vessel_torpedoed[i]:
                    vessels[i].log("WARNING: Vessel torpedoed!")
                else:
                    vessels[i].log("WARNING: Vessel destroyed in collision!")
                vessels[i].make_wreck()

def game_loop(server_socket, logfile_directory):
    t = 0
    time_between_turns = 30
    delta_t = 0.2
    assert time_between_turns/delta_t == math.floor(time_between_turns/delta_t)
    vessels = []
    time = lambda x=None : t
    while t >= 0:
        # look for new vessels
        if len(vessels) == 0:
            print("Waiting for players ...")
        while True:
            try:
                (client_socket, address) = server_socket.accept()
                print("\nNew connection ... ", end='', flush=True)
                print(handle_new_connection(vessels, client_socket, time, logfile_directory))
            except BlockingIOError:
                sleep(0.01)
                if len(vessels) > 0:
                    break
                else:
                    continue
        # prune useless wrecks / empty torpedoes
        for i, vessel in enumerate(vessels):
            if vessel.num_seconds_left != None and vessel.num_seconds_left <= 0:
                print("Pruning vessel:", vessels[i].name)
                vessels.pop(i).cleanup()
        # output
        print("[%ds] Vessels: " % t, ", ".join(formatted_list(vessels)))
        for vessel in vessels:
            if vessel.output_file != None:
                vessel.write_output()
            vessel.process_contacts(vessels)
        # read in commands
        for vessel in vessels:
            print(
                "\tTurn:", vessel.name,
                "contacts:", list(map(math.degrees, vessel.sonar_contacts)),
                file=sys.stderr
            )
            vessels += vessel.get_input() # run AI routines / get user input
        for i in range(int(time_between_turns / delta_t)):
            for vessel in vessels:
                if vessel.socket == None and vessel.vessel_type == VESSEL_TORPEDO:
                    vessel.process_contacts(vessels)
                    vessel.get_input_ai()
                vessel.simulate_time(delta_t)
                check_for_collisions(vessels)
        t += time_between_turns

if __name__ == "__main__":
    if (len(sys.argv) < 2) or ("-h" in sys.argv) or ("--help" in sys.argv):
        print("Usage: %s logfile_directory" % sys.argv[0])
        exit(len(sys.argv) < 2)
    else:
        address = sys.argv[1] + "/server.socket"
        print("Starting server on UDS", address)

        server_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            os.unlink(address)
        except FileNotFoundError:
            pass
        try:
            server_socket.bind(address)
        except FileNotFoundError:
            print("ERROR: Bad logfile directory: %s" % sys.argv[1])
            exit(1)
        server_socket.setblocking(False)
        server_socket.listen()
        try:
            game_loop(server_socket, sys.argv[1])
        except KeyboardInterrupt:
            print("Shutting down server.")
            server_socket.close()
            os.unlink(address)

